# 1. Create 5 variables and output them in the command prompt in the following format:
# "I am < name (string)> , and I am < age (integer)> years old, I work as a < occupation (string)> , and my rating for < movie (string)> is < rating (decimal)> %"

# variables
name = "Jose"
age = 38
occupation = "writer"
movie = "One more chance"
rating = 99.6

# output
print(f"I am {name}, and I am {age} years old, I work as a {occupation}, and my rating for {movie} is {rating}%")




# 2. Create 3 variables, num1, num2, and num3 
num1 = 5
num2 = 10
num3 = 7

# a. Get the product of num1 and num2
product = num1 * num2
# b. Check if num1 is less than num3
is_less = num1 < num3
# c. Add the value of num3 to num2
num2 += num3

# output
print("Product of num1 and num2:", product)
print("Is num1 less than num3?", is_less)
print("Value of num2 after adding num3:", num2)